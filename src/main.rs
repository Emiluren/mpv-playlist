use gtk::prelude::*;
use gio::prelude::*;

fn main() {
    let application = gtk::Application::new(
        None,
        Default::default(),
    ).expect("failed to initialize GTK application");

    application.connect_activate(|app| {
        let window = gtk::ApplicationWindow::new(app);
        window.set_title("GTK test");

        let listbox = gtk::ListBox::new();
        window.add(&listbox);

        let add_row = |name| {
            let row = gtk::ListBoxRow::new();
            let rowbox = gtk::Box::new(gtk::Orientation::Horizontal, 0);
            row.add(&rowbox);
            listbox.add(&row);

            let handle = gtk::EventBox::new();
            handle.add(&gtk::Image::from_icon_name(Some("open-menu-symbolic"), gtk::IconSize::Dnd));
            rowbox.add(&handle);

            let label = gtk::Label::new(Some(name));
            rowbox.set_center_widget(Some(&label));

            let targets = [
                gtk::TargetEntry::new("GTK_LIST_BOX_ROW", gtk::TargetFlags::SAME_APP, 0)
            ];

            handle.drag_source_set(gdk::ModifierType::BUTTON1_MASK, &targets, gdk::DragAction::MOVE);
            row.drag_dest_set(gtk::DestDefaults::ALL, &targets, gdk::DragAction::MOVE);
        };
        add_row("Test");
        add_row("Test 2");

        let button = gtk::Button::with_label("Click me!");
        button.connect_clicked(|_| {
            println!("Clicked!");
        });
        listbox.add(&button);

        window.show_all();
    });

    application.run(&[]);
}
